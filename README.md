# README

### Задание:
https://gitlab.com/shakuro-public/backend-test

### Мое:<br/>
app/controllers<br/>
app/models<br/>
db/migrate<br/>
db/seeds.rb<br/>
spec/requests<br/>

Заранее прошу прощение за качество кода и разбиение кода по файлам.<br/>
Из-за прыжков с языка на язык оно страдает, буду исправлять.<br/>
Со спеками труба. Сделал заполнение через seed.rb, хоть это и так себе вариант.

Добавлена таблица продаж Sales.<br/>
Нет работы с ценами, нет логов, но их нет в задаче.<br/>

### Для поиграться можно залить данные из seed.rb

Пример с набором данных:
curl -X GET "http://localhost:3000/publisher/2/shops"

Магазины с товарами:
(shop_id, book_id, qty)
11 22 1
11 23 5
13 26 6
14 25 2
14 26 10

Примеры покупки:
curl -X POST -d "book_id=22" "http://localhost:3000/shop/11/sell"
curl -X POST -d "book_id=23&quantity=2" "http://localhost:3000/shop/11/sell"