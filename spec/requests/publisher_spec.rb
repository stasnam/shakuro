require 'rails_helper'

RSpec.describe 'ShopController', type: :request do
  describe 'GET publisher/:id/shops' do
    before(:each) do
      @empty_answer = { shops: [] }.to_json

      @publisher = Publisher.create(name: 'Publisher 1')
      # @shop = Shop.create(name: 'Shop 1')
      # @book = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 1')
      # @storage = ShopStorage.create(shop_id: @shop.id, book_id: @book.id, quantity: 10)
      # @sale = Sale.create(shop_id: @shop.id, book_id: @book.id, quantity: 7)
    end

    it 'publisher not exists' do
      publisher_id =  @publisher.id + 1
      get "/publisher/#{publisher_id}/shops"
      expect(response.body).to eq @empty_answer
    end

    it 'publisher without books' do
      publisher = Publisher.create(name: 'Publisher 2')
      get "/publisher/#{publisher.id}/shops"
      expect(response.body).to eq @empty_answer
    end

    it 'all books sold' do
      # Have sales, but no books in stock
      book1 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 1')
      book2 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 2')
      shop = Shop.create(name: 'Shop 1')
      ShopStorage.create(shop_id: shop.id, book_id: book1.id, quantity: 0)
      ShopStorage.create(shop_id: shop.id, book_id: book2.id, quantity: 0)
      Sale.create(shop_id: shop.id, book_id: book1.id, quantity: 7)

      get "/publisher/#{@publisher.id}/shops"
      expect(response.body).to eq @empty_answer
    end

    it 'complex test' do
      def fill_other_shops(shops)
        # В соседнем магазине есть в наличии и в продаже книги другого издательства
        shops.values.each do |shop|
          ShopStorage.create(shop_id: shop.id, book_id: @p2_book1.id, quantity: 6)
          ShopStorage.create(shop_id: shop.id, book_id: @p2_book2.id, quantity: 7)
          Sale.create(shop_id: shop.id, book_id: @p2_book1.id, quantity: 2)
        end
      end

      # TODO: причесать сию простыню.
      publisher2 = Publisher.create(name: 'Publisher 2')

      shops = {}
      (1..6).each do |i|
        shops[i] = Shop.create(name: "Shop #{i}")
      end

      book1 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 1')
      book2 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 2')
      book3 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 3')
      @p2_book1 = Book.create(publisher_id: publisher2.id, name: 'Pub 2 Book 1')
      @p2_book2 = Book.create(publisher_id: publisher2.id, name: 'Pub 2 Book 2')

      # Во все магазины добавим книги другого издательства
      fill_other_shops(shops)

      # Магазин 1. Нет книг от нужного паблишера, нет продаж от издательства, skip

      # Магазин 2. Нет книг от нужного паблишера, есть продажи, skip
      Sale.create(shop_id: shops[2].id, book_id: book1.id, quantity: 3)
      Sale.create(shop_id: shops[2].id, book_id: book1.id, quantity: 4)
      Sale.create(shop_id: shops[2].id, book_id: book3.id, quantity: 5)

      # Магазин 3. Книги есть, но все проданы, skip
      ShopStorage.create(shop_id: shops[3].id, book_id: book1.id, quantity: 0)
      ShopStorage.create(shop_id: shops[3].id, book_id: book2.id, quantity: 0)
      Sale.create(shop_id: shops[3].id, book_id: book1.id, quantity: 3)
      Sale.create(shop_id: shops[3].id, book_id: book1.id, quantity: 4)
      Sale.create(shop_id: shops[3].id, book_id: book3.id, quantity: 5)

      # Магазин 4. Одной книги нет, одна распродана, одна с остатком, продаж нет
      #            ассортимент 1, остаток 6, продаж 0
      ShopStorage.create(shop_id: shops[4].id, book_id: book1.id, quantity: 0)
      ShopStorage.create(shop_id: shops[4].id, book_id: book2.id, quantity: 6)

      # Магазин 5. Одной книги нет, одна распродана, одна с остатком, продажи есть
      #            ассортимент 2, остаток 12, продаж 10
      ShopStorage.create(shop_id: shops[5].id, book_id: book1.id, quantity: 2)
      ShopStorage.create(shop_id: shops[5].id, book_id: book2.id, quantity: 10)
      ShopStorage.create(shop_id: shops[5].id, book_id: book3.id, quantity: 0)
      Sale.create(shop_id: shops[5].id, book_id: book1.id, quantity: 1)
      Sale.create(shop_id: shops[5].id, book_id: book1.id, quantity: 3)
      Sale.create(shop_id: shops[5].id, book_id: book3.id, quantity: 2)
      Sale.create(shop_id: shops[5].id, book_id: book3.id, quantity: 4)

      # Магазин 6. пара книг, продажи есть. Нужна для проверки сортировки.
      #            ассортимент 2, остаток 2, продаж 5
      ShopStorage.create(shop_id: shops[6].id, book_id: book1.id, quantity: 1)
      ShopStorage.create(shop_id: shops[6].id, book_id: book2.id, quantity: 1)
      ShopStorage.create(shop_id: shops[6].id, book_id: book3.id, quantity: 1)
      Sale.create(shop_id: shops[6].id, book_id: book1.id, quantity: 5)

      get "/publisher/#{@publisher.id}/shops"

      result = JSON.parse(response.body)
      expected = [
        { id: shops[5].id, name: 'Shop 5', sold: 10, assortiment: 2, in_stock: 12 },
        { id: shops[6].id, name: 'Shop 6', sold: 5, assortiment: 3, in_stock: 3 },
        { id: shops[4].id, name: 'Shop 4', sold: 0, assortiment: 1, in_stock: 6 }
      ]
      shops = result['shops']
      expect(shops.count).to eq 3

      expected.each_with_index do |row, idx|
        shop_data = shops[idx]
        in_stock = shop_data['books_in_stock'].sum { |obj| obj['copies_in_stock']}

        expect(shop_data['id']).to eq row[:id]
        expect(shop_data['name']).to eq row[:name]
        expect(shop_data['books_sold_count']).to eq row[:sold]
        expect(shop_data['books_in_stock'].count).to eq row[:assortiment]
        expect(in_stock).to eq row[:in_stock]
      end
    end
  end
end
