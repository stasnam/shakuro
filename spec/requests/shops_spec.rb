require 'rails_helper'

RSpec.describe 'ShopController', type: :request do
  describe 'POST /shop/:shop_id/sell' do
    before(:each) do
      @publisher = Publisher.create(name: 'Publisher 1')
      @shop = Shop.create(name: 'Shop 1')
      @book = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 1')
      @storage = ShopStorage.create(shop_id: @shop.id, book_id: @book.id, quantity: 10)
      @sale = Sale.create(shop_id: @shop.id, book_id: @book.id, quantity: 7)
      @shop_id = @shop.id
      @book_id = @book.id
    end

    def buy_and_check_response(status, excepted_json, quantity=nil)
      params = { book_id: @book_id }
      params[:quantity] = quantity unless quantity.nil?

      post "/shop/#{@shop_id}/sell", params: params

      expect(response.status).to eq status
      expect(response.body).to eq excepted_json.to_json
    end

    context 'Success sales' do
      def buy_and_check_success(quantity=nil)
        excepted_json = { result: 'OK' }
        buy_and_check_response(200, excepted_json, quantity)
      end

      def check_storage_sales(storage_qty, last_sale_qty)
        storage = ShopStorage.find_by(shop_id: @shop_id, book_id: @book_id)
        expect(storage.quantity).to eq storage_qty
        sale = Sale.where(shop_id: @shop_id, book_id: @book_id).order('created_at desc').first
        expect(sale.quantity).to eq last_sale_qty
      end

      it 'without quantity' do
        # Buy 1 book. Result balance (10-1)
        buy_and_check_success
        check_storage_sales(@storage.quantity-1, 1)
      end

      it 'with quantity' do
        # Buy all books. Result balance 0.
        buy_and_check_success(@storage.quantity)
        check_storage_sales(0, @storage.quantity)
      end
    end

    context 'Bad requests' do
      it 'book_id is not integer' do
        @book_id = '23qw'
        excepted = { result: 'ERROR', error: "Bad book_id (23qw)." }

        buy_and_check_response(400, excepted)
      end

      it 'bad quantity (-3)' do
        excepted = { result: 'ERROR', error: 'Bad quantity. (quantity: -3)' }
        buy_and_check_response(400, excepted, -3)
      end

      it 'bad quantity (12we)' do
        excepted = { result: 'ERROR', error: 'Bad quantity. (quantity: 12we)' }
        buy_and_check_response(400, excepted, '12we')
      end

      it 'shop not found' do
        @shop_id = 999
        excepted = {
            result: 'ERROR',
            error: "Book or shop not found. (book_id: #{@book_id}, shop_id: #{@shop_id})"
        }
        buy_and_check_response(404, excepted)
      end

      it 'book not found' do
        @book_id = 999
        excepted = { result: 'ERROR', error: "Book or shop not found. (book_id: #{@book_id}, shop_id: #{@shop_id})" }

        buy_and_check_response(404, excepted)
      end

      it 'book not present in this shop' do
        book2 = Book.create(publisher_id: @publisher.id, name: 'Pub 1 Book 2')
        @book_id = book2.id
        excepted = { result: 'ERROR', error: "Book or shop not found. (book_id: #{@book_id}, shop_id: #{@shop_id})" }
        buy_and_check_response(404, excepted)
      end

      it 'try buy more than have in stock' do
        excepted = { result: 'ERROR', error: 'Not enough books in stock. (ordered: 11, allowed: 10)' }
        buy_and_check_response(400, excepted, 11)
      end
    end
  end
end
