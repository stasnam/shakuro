Rails.application.routes.draw do
  get 'publisher/:id/shops', to: 'publisher#shops', constraints: { id: /\d+/ }
  post 'shop/:shop_id/sell', to: 'shop#sell', constraints: { shop_id: /\d+/ }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
