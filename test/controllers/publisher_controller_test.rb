require "test_helper"

class PublisherControllerTest < ActionDispatch::IntegrationTest
  test "should get shops" do
    get publisher_shops_url
    assert_response :success
  end
end
