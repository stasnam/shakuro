require "test_helper"

class ShopControllerTest < ActionDispatch::IntegrationTest
  test "book_id is empty" do
    post "/shop/1/sell"

    expected = {"result" => "ERROR", "error" => "Book in shop not found. (book_id: , shop_id: 1)"}

    assert(response.status, 404)
    assert(response.body, expected.to_json)
  end

  test "without quantity" do
    post "/shop/1/sell", params: {book_id: 3}

    expected = {"result" => "ERROR", "error" => "Book in shop not found. (book_id: , shop_id: 1)"}

    assert(response.status, 404)
    assert(response.body, expected.to_json)
  end

end
