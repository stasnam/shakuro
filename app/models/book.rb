class Book < ApplicationRecord
  belongs_to :publisher
  has_many :sales
  has_many :shop_storages
end
