class Shop < ApplicationRecord
  has_many(:shop_storages)
  has_many(:sales)
end
