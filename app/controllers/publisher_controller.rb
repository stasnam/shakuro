# PublisherController
class PublisherController < ApplicationController

  def _prepare_shops(publisher_id)
    res = Book.joins(shop_storages: [:shop])
              .select('books.id, books.name, shop_storages.shop_id, shop_storages.quantity, shops.name as shop_name')
              .where(['publisher_id = ? and shop_storages.quantity > 0', publisher_id]).order('shop_storages.shop_id')

    shops = {}
    book_ids = Set[]
    res.each do |row|
      unless shops.key?(row.shop_id)
        shops[row.shop_id] = {
          id: row.shop_id,
          name: row.shop_name,
          books_sold_count: 0,
          books_in_stock: []
        }
      end

      book_ids.add(row.id)

      book = {
        id: row.id,
        title: row.name,
        copies_in_stock: row.quantity
      }
      shops[row.shop_id][:books_in_stock].append(book)
    end

    [book_ids, shops]
  end

  def _get_total_sales(book_ids, shop_ids)
    Sale.select('shop_id, sum(quantity) ss')
        .where(['book_id in (?) and shop_id in (?)', book_ids, shop_ids])
        .group('shop_id')
        .order('ss desc')
  end

  def shops
    publisher_id = params[:id]

    # Магазины, где есть книги издателя в продаже
    book_ids, shops = _prepare_shops(publisher_id)

    # По магазинам, где есть книги собираем все продажи от данного паблишера.
    # Тут считаем, что нас интересуют полные продажи, а не продажи книг, которые есть в магазине.
    res = _get_total_sales(book_ids, shops.keys)

    result = { shops: [] }
    res.each do |row|
      shops[row.shop_id]['books_sold_count'] = row.ss
      result[:shops].append(shops[row.shop_id])
      shops.delete(row.shop_id)
    end
    result[:shops].push(*shops.values) # shop without sales

    output = result.to_json
    render json: output
  end
end
