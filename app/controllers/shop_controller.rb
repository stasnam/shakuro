class ShopController < ApplicationController
  def sell
    shop_id = params[:shop_id]
    book_id = params[:book_id]

    if book_id.to_i.to_s != book_id
      render_error("Bad book_id (#{book_id}).")
      return
    end

    if params[:quantity].nil?
      quantity = 1
    else
      # Если параметр не пустой, то валидируем, default не используем.
      quantity = params[:quantity].to_i
      if quantity < 1 || quantity.to_s != params[:quantity]
        render_error("Bad quantity. (quantity: #{params[:quantity]})")
        return
      end
    end

    # Если нужны более подробные ошибки, то нужно сперва запросить в БД book и shop
    # Вcё ок, списываем. (проверка, что в минус уйти не можем на уровне PG.)
    begin
      ActiveRecord::Base.transaction do
        storage_row = ShopStorage.find_by(shop_id: shop_id, book_id: book_id)

        unless storage_row
          render_error("Book or shop not found. (book_id: #{book_id}, shop_id: #{shop_id})", 404)
          return
        end

        # В принципе, не обязательно, т.к. есть проверка на уровне БД. Но так приличнее.
        if storage_row.quantity < quantity
          render_error("Not enough books in stock. (ordered: #{quantity}, allowed: #{storage_row.quantity})")
          return
        end

        storage_row.lock!
        storage_row.quantity -= quantity
        storage_row.save!
        Sale.create!(shop_id: shop_id, book_id: book_id, quantity: quantity)
      end
    rescue ActiveRecord::StatementInvalid
      render_error("Can't buy!")
      return
    end

    output = {result: 'OK'}.to_json
    render json: output
  end
end
