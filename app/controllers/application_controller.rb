class ApplicationController < ActionController::API
  def render_error(message, code=400)
    render json: { result: 'ERROR', error: message }.to_json, status: code
  end
end
