class ShopStorageConstraint < ActiveRecord::Migration[6.1]
  def up
    add_check_constraint :shop_storages, 'quantity >= 0', name: 'quantity_non_negative'
  end

  def down
    remove_check_constraint  :shop_storages, name: 'quantity_non_negative'
  end
end
