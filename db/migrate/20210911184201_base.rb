class Base < ActiveRecord::Migration[6.1]
  # TODO: почитать можно ли FK и constrainr через change
  def change
    create_table :publishers do |t|
      t.string :name, null: false
      t.timestamps
    end

    create_table :books do |t|
      t.belongs_to :publisher, null: false, index: true
      t.string :name, null: false
      t.timestamps
    end
    add_foreign_key :books, :publishers, on_delete: :restrict

    create_table :shops do |t|
      t.string :name, null: false
      t.timestamps
    end

    create_table :shop_storages do |t|
      t.belongs_to :shop, null: false, index: false # дублирует unique (shop+book_id)
      t.belongs_to :book, null: false, index: true # для поиска по издательствам
      t.integer :quantity, null: false
      t.timestamps
      t.index %w(shop_id book_id), unique: true
    end
    add_foreign_key :shop_storages, :shops, on_delete: :restrict
    add_foreign_key :shop_storages, :books, on_delete: :restrict

    create_table :sales do |t|
      t.belongs_to :shop, null: false, index: true
      t.belongs_to :book, null: false, index: true
      t.integer :quantity, null: false
      t.timestamps
    end
    add_foreign_key :sales, :shops, on_delete: :restrict
    add_foreign_key :sales, :books, on_delete: :restrict
  end
end
