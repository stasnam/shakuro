# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Publisher.create(id: 1, name: 'Publisher 1')
Publisher.create(id: 2, name: 'Publisher 2')
Publisher.create(id: 3, name: 'Publisher 3')
Publisher.create(id: 4, name: 'Publisher 4')

Shop.create(id: 11, name: 'Shop 1')
Shop.create(id: 12, name: 'Shop 2')
Shop.create(id: 13, name: 'Shop 3')
Shop.create(id: 14, name: 'Shop 4')
Shop.create(id: 15, name: 'Shop 5')

Book.create(id: 21, publisher_id: 1, name: 'Pub 1 Book 1')
Book.create(id: 22, publisher_id: 1, name: 'Pub 1 Book 2')
Book.create(id: 23, publisher_id: 1, name: 'Pub 1 Book 3')
Book.create(id: 24, publisher_id: 2, name: 'Pub 2 Book 1')
Book.create(id: 25, publisher_id: 2, name: 'Pub 2 Book 2')
Book.create(id: 26, publisher_id: 2, name: 'Pub 2 Book 3')
Book.create(id: 27, publisher_id: 3, name: 'Pub 3 Book 1')
Book.create(id: 28, publisher_id: 3, name: 'Pub 3 Book 2')
Book.create(id: 29, publisher_id: 3, name: 'Pub 3 Book 3')

ShopStorage.create(shop_id: 11, book_id: 21, quantity: 0)
ShopStorage.create(shop_id: 11, book_id: 22, quantity: 1)
ShopStorage.create(shop_id: 11, book_id: 23, quantity: 5)
ShopStorage.create(shop_id: 11, book_id: 27, quantity: 0)
ShopStorage.create(shop_id: 12, book_id: 21, quantity: 0)
ShopStorage.create(shop_id: 12, book_id: 24, quantity: 0)
ShopStorage.create(shop_id: 12, book_id: 25, quantity: 0)
ShopStorage.create(shop_id: 12, book_id: 28, quantity: 0)
ShopStorage.create(shop_id: 13, book_id: 24, quantity: 0)
ShopStorage.create(shop_id: 13, book_id: 25, quantity: 0)
ShopStorage.create(shop_id: 13, book_id: 26, quantity: 6)
ShopStorage.create(shop_id: 14, book_id: 24, quantity: 0)
ShopStorage.create(shop_id: 14, book_id: 25, quantity: 2)
ShopStorage.create(shop_id: 14, book_id: 26, quantity: 10)
ShopStorage.create(shop_id: 15, book_id: 24, quantity: 1)
ShopStorage.create(shop_id: 15, book_id: 25, quantity: 1)
ShopStorage.create(shop_id: 15, book_id: 26, quantity: 1)

Sale.create(shop_id: 11, book_id: 21, quantity: 2)
Sale.create(shop_id: 11, book_id: 21, quantity: 3)
Sale.create(shop_id: 11, book_id: 27, quantity: 3)
Sale.create(shop_id: 12, book_id: 24, quantity: 30)
Sale.create(shop_id: 14, book_id: 24, quantity: 1)
Sale.create(shop_id: 14, book_id: 24, quantity: 3)
Sale.create(shop_id: 14, book_id: 26, quantity: 2)
Sale.create(shop_id: 14, book_id: 26, quantity: 4)
Sale.create(shop_id: 15, book_id: 24, quantity: 5)


